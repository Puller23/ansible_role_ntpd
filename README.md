# Ansible Role: ntpd

Installs NTP

## Requirements

None.

## Role Variables
  
Set the restrict parameter
    ntp_restrict: ['nomodify', 'notrap', 'nopeer', 'noquery']

Define NTP Server
    ntp_server:
    -  8.8.8.8

## Dependencies

None.

## Example Playbook

    - hosts: all
      roles:
        - ntpd

## Example vars

    ntp_restrict: ['nomodify', 'notrap', 'nopeer', 'noquery']
    ntp_server:
    -  172.29.112.11
    -  172.29.112.12
    -  172.29.112.13


## License

Apache-2.0

## Author Information

This role was created in 2020 by Gregor Bartels
